# Task

# Summary
Stack:
* NodeJS
* Typescript
* TypeORM
* NestJS with Express routing underlying

Doing it in `Express` might have saved me a lot of time but I wanted to explore some new stuff while doing the task. 
Although this is the first time I am using `NestJS`, I found it very compact and very much compatible with typescript style architecture. 
It's growing fast and I believe it's worth a try. For More: https://github.com/nestjs/nest

Didn't use GraphQL cause I used it long time ago and `NestJS` is already a new stuff for me.

## Installation
Requirements:
* Node v10+
* PostgreSQL
* Typescript

```
## Local Machine
## update DB credential in .env

npm install
npm run start:dev


## Docker
## no need to update .env

docker-compose up -d --build
```
The app will be running at  : http://127.0.0.1:3000
Please make sure port `3000` is free

You don't need to run any migration as `.evn` has 
```
TYPEORM_SYNCHRONIZE=true
```
If you disable this. Please run the migration manually
```
npm run migration
```

## Mini documentation
### Upload csv file
CSV file should contain bellow headers and contain valid data
```
apartment_id,zip_code,apartment_type,apartment_size,rent,deleted
7ee8e00e-8cf2-4bc1-8cfc-8d45ed971557,10179,garden,71,700,false
```

| Name             | Description  |
| :------------ | ------------: |
| Endpoint     | POST /apartments/upload/  |
| Content Type  | multipart/form-data  |
| input field name   | file     |

Fileupload Screenshot by PostMan:
![](file-upload-screenshot.png)
----------------------------------------------

### Listing apartments
List 50 apartments in page 1
```
GET /apartments
```

List 50 apartments in area `10319`
```
page 1: GET /apartments?zipCode=10319
page 2: GET /apartments?zipCode=10319&page=2
```

List apartment with following filters:
* Apartment size more than `50` square meter
* Apartment type `penthouse` or `studio`
```
GET apartments?apartmentSize[MoreThan]=50&apartmentType[Any]=studio,penthouse&zipCode=10319
```

## Estimate apartment rent 
Estimate with `zipCode` and `apartmentSize`
```
GET apartments/estimateRent?zipCode=10318&apartmentSize=100
```

Estimate with `zipCode`, `apartmentSize` and `apartmentType`
```
GET apartments/estimateRent?zipCode=10318&apartmentSize=100&apartmentType=penthouse
```

# Test
For integration test, we need a test database up running first.
```
docker-compose up -d --build testdb
```
Then execute
```
npm test
```

#  Further Improvement
For sure this solution doesn't have many important building blocks for production applications and also can be improved many implementation in many different way. I tried to kept everything minimal for the sake of the task and time.

### Feature Improvement
* Currently file upload is not synchronous. For large files it might take some time to upload the data and its not a good idea to let user wait. It should upload in background and notify the result later.
* create a `cmd` to import file directly from console along with API
* Implements monitoring, logging to ship to log services (i.e datadog, newrelic etc)

### Testing Improvement
* Add more tests to cover edge cases
* Run Mock DB SERVER instead of docker test DB

### Tooling Improvement
* Improve build pipeline for production

### Code Structure
This is my first project in `NestJS`. I think there could be a lot of different of way of improving it. But surely, I don't regret doing it this way for the moment.

---------

Thanks for your time reviewing the code.
