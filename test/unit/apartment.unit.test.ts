import { ApartmentService } from '../../src/modules/apartment/apartment.service';
import { EntityManager, QueryRunner, Repository } from 'typeorm';
import { Apartment } from '../../src/modules/apartment/apartment.model';
import { ApartmentController } from '../../src/modules/apartment/apartment.controller';
import { SearchQuery } from '../../src/query/SearchQuery';
import { Pagination } from '../../src/query/Pagination';
import { readFileSync } from 'fs';
import { EstimateApartmentDto } from '../../src/modules/apartment/dto/estimate.dto';

describe('Unit Test', () => {
    let repository: Repository<Apartment>;
    let apartmentService: ApartmentService;
    let apartmentController: ApartmentController;

    beforeEach(() => {
        repository = {
            findAndCount: () => undefined,
            manager: {
                query: () => 0,
                save: () => 0,
                connection: {
                    createQueryRunner: () => 0,
                },
            } as unknown as EntityManager,
        } as Repository<Apartment>;

        apartmentService = new ApartmentService(repository);
        apartmentController = new ApartmentController(apartmentService);
    });

    describe('ApartmentService', () => {
        it('should return an array of apartments with counts', async () => {
            const mockData = readFileSync(__dirname + '/../data/apartment-list-mock.unit.json').toString();
            const rows = JSON.parse(mockData) as Apartment[];

            jest.spyOn(repository, 'findAndCount').mockImplementationOnce(() => Promise.resolve([rows, 2]));

            const searchQuery = { condition: {}, pagination: new Pagination() } as SearchQuery;
            const outputJSON = readFileSync(__dirname + '/../data/apartment-list-response.unit.json').toString();
            const expectedResponse = JSON.stringify(JSON.parse(outputJSON));
            const response = await apartmentController.apartmentList(undefined, searchQuery);

            expect(JSON.stringify(response)).toBe(expectedResponse);
        });

        it('should save all the records', async () => {
            const mockData = readFileSync(__dirname + '/../data/apartment-list-mock.unit.json').toString();
            const rows = JSON.parse(mockData) as Apartment[];
            const queryRunner = {
                manager: {save: () => undefined},
                startTransaction: () => Promise.resolve(),
                commitTransaction: () => Promise.resolve(),
                rollbackTransaction: () => Promise.resolve(),
            } as unknown as QueryRunner;

            jest.spyOn(repository.manager.connection, 'createQueryRunner').mockImplementationOnce(() => queryRunner);

            const expectedResponse = `{"status":"success","message":"File imported successfully","data":{}}`;
            const response = await apartmentController.uploadFile(rows);
            expect(JSON.stringify(response)).toBe(expectedResponse);
        });

        it('should estimate the rent', async () => {
            jest.spyOn(repository.manager, 'query').mockResolvedValue([{price_per_sqm: 9}]);

            const dto = {apartmentSize: 55, apartmentType: '', zipCode: 12222} as EstimateApartmentDto;
            const rent = await apartmentService.estimateRent(dto);
            const expectedResponse = `{"apartmentSize":55,"rentPerSquareMeter":9,"estimatedRent":495}`;

            expect(JSON.stringify(rent)).toBe(expectedResponse);
        });

        it('should throw an error to estimate', async () => {
            jest.spyOn(repository.manager, 'query').mockResolvedValue([{}]);
            const dto = {apartmentSize: 55, apartmentType: '', zipCode: 12222} as EstimateApartmentDto;

            try {
                await apartmentService.estimateRent(dto);
                // Fail test if above expression doesn't throw anything.
                expect(true).toBe(false);
            } catch (e) {
                expect(e.response.message).toBe('Unable to estimateRent the rent with given parameters');
            }
        });
    });
});
