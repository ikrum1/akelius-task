import request from 'supertest';
import { readFileSync } from 'fs';

import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';
import { AppModule } from '../../src/modules/app.module';
import { AppController } from '../../src/modules/app.controller';
import { AppService } from '../../src/modules/app.service';
import { seedTestDB, testDBConfig } from '../util/db';

describe('Apartments', () => {
    let app: INestApplication;

    beforeAll(async () => {
        await seedTestDB();
        const dbConfig = Object.assign(testDBConfig(), {
            type: 'postgres',
            entities: ['src/**/*.model.{ts,js}'],
            namingStrategy: new SnakeNamingStrategy(),
        });

        const moduleRef = await Test.createTestingModule({
            imports: [
                ConfigModule.forRoot(),
                TypeOrmModule.forRoot(dbConfig),
                AppModule,
            ],
            controllers: [AppController],
            providers: [AppService],
        })
            .compile();

        app = moduleRef.createNestApplication();
        await app.init();
    });

    it(`GET /apartments`, () => {
        return request(app.getHttpServer())
            .get('/apartments')
            .expect(200)
            .then(response => {
                const testData = readFileSync(__dirname + '/../data/apartment-list.e2e.json').toString();
                const expectedOutput = JSON.stringify(JSON.parse(testData));
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`GET /apartments?zipCode=10318`, () => {
        return request(app.getHttpServer())
            .get('/apartments?zipCode=10318')
            .expect(200)
            .then(response => {
                const testData = readFileSync(__dirname + '/../data/apartment-list-with-zipcode-filter.e2e.json').toString();
                const expectedOutput = JSON.stringify(JSON.parse(testData));
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`GET /apartments/estimateRent without query param`, () => {
        return request(app.getHttpServer())
            .get('/apartments/estimateRent')
            .expect(400)
            .then(response => {
                const expectedOutput = `{"statusCode":400,"error":"Bad Request","message":"Missing required query parameters"}`;
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`GET /apartments/estimateRent?zipCode=10318&apartmentSize=100`, () => {
        return request(app.getHttpServer())
            .get('/apartments/estimateRent?zipCode=10318&apartmentSize=100')
            .expect(200)
            .then(response => {
                const testData = readFileSync(__dirname + '/../data/estimate.e2e.json').toString();
                const expectedOutput = JSON.stringify(JSON.parse(testData));
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`POST /apartments/upload`, () => {
        return request(app.getHttpServer())
            .post('/apartments/upload')
            .attach('file', __dirname + '/../data/valid-rents-data.e2e.csv')
            .expect(201)
            .then(response => {
                const expectedOutput = `{"status":"success","message":"File imported successfully","data":{}}`;
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`POST /apartments/upload with invalid csv`, () => {
        return request(app.getHttpServer())
            .post('/apartments/upload')
            .attach('file', __dirname + '/../data/invalid-rents-data.e2e.csv')
            .expect(400)
            .then(response => {
                const expectedOutput = `{"statusCode":400,"error":"Invalid data at line 2","message":"Value for apartment_id is invalid"}`;
                expect(response.text).toBe(expectedOutput);
            });
    });

    it(`POST /apartments/upload with empty file`, () => {
        return request(app.getHttpServer())
            .post('/apartments/upload')
            .attach('file', __dirname + '/../data/empty-file.e2e.csv')
            .expect(400)
            .then(response => {
                const expectedOutput = `{"statusCode":400,"error":"Bad Request","message":"Empty file is not allowed"}`;
                expect(response.text).toBe(expectedOutput);
            });
    });

    afterAll(async () => {
        await app.close();
    });
});
