import Postgrator from 'postgrator';
import { readFileSync } from 'fs';

const seedQuery = readFileSync(__dirname + '/../data/test-db-seed.e2e.sql').toString();

export function testDBConfig(): any {
    return {
        host: 'localhost',
        port: 5444,
        username: 'postgres',
        password: 'postgres',
        database: 'postgres',
    };
}

export async function seedTestDB(): Promise<void> {
    const connection = new Postgrator(Object.assign({ driver: 'pg' }, testDBConfig()));
    await connection.runQuery(seedQuery);
}
