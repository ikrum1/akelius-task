CREATE TABLE IF NOT EXISTS "apartments" (
    "apartment_id" character varying NOT NULL PRIMARY KEY,
    "zip_code" integer NOT NULL,
    "apartment_type" character varying NOT NULL,
    "apartment_size" integer NOT NULL,
    "rent" integer NOT NULL,
    "created_at" timestamp NOT NULL default NOW(),
    "updated_at" timestamp NULL,
    "deleted" boolean NOT NULL DEFAULT '0'
);
