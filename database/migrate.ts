import Postgrator from 'postgrator';
import { config } from 'dotenv';

config();

const postgrator = new Postgrator({
    migrationDirectory: __dirname + '/migrations',
    driver: 'pg',
    host: process.env.TYPEORM_HOST,
    port: Number(process.env.TYPEORM_PORT),
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    schemaTable: 'migrations',
});

postgrator
    .migrate()
    .then(appliedMigrations => {
        if (appliedMigrations.length === 0) {
            return console.log('=========== No Migration Applied ===========\n\n');
        }

        console.log('=========== Migration Applied ===========');
        for (const migration of appliedMigrations) {
            console.log('\t-', migration.filename);
        }

        console.log('=========== Migration Successful ===========\n\n');
    })
    .catch(error => console.log(error));
