export interface QueryResult {
    affectedRows?: number;
    error?: Error;
    rows?: object[];
    count?: number;
}
