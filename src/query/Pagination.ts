export class Pagination {
    public page: number;
    public limit: number;
    public offset: number;
}
