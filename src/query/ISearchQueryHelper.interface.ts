import {SearchQuery} from './SearchQuery';

export interface ISearchQueryHelper {
    getSearchQuery(): SearchQuery;
}
