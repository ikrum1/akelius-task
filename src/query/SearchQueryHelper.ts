import { Pagination } from './Pagination';
import { SearchQuery } from './SearchQuery';
import { Any, Equal, LessThan, LessThanOrEqual, Like, MoreThan, MoreThanOrEqual, Not } from 'typeorm';
import {ISearchQueryHelper} from './ISearchQueryHelper.interface';

export class SearchQueryHelper implements ISearchQueryHelper {

    public readonly DEFAULT_QUERY_LIMIT = 50;
    public readonly DEFAULT_PAGE = 1;

    constructor (private searchableFields: string[], private query: object) {
    }

    public getSearchQuery(): SearchQuery {
        return {
            condition: this.buildQueryCondition(),
            pagination: this.buildPagination(),
        } as SearchQuery;
    }

    private buildPagination(): Pagination {
        const limit = Number(this.query['limit']) > 0 ? Number(this.query['limit']) : this.DEFAULT_QUERY_LIMIT;
        const page = Number(this.query['page']) > 0 ? Number(this.query['page']) : this.DEFAULT_PAGE;
        const offset = (page - 1) * this.DEFAULT_QUERY_LIMIT;

        return {
            limit,
            offset,
            page,
        } as Pagination;
    }

    private buildQueryCondition(): object {
        const condition = {};
        for (const field of this.searchableFields) {
            if (!this.query[field]) {
                continue;
            } else if (typeof this.query[field] === 'string') {
                condition[field] = this.query[field];
            } else if (typeof this.query[field] === 'object') {
                const ops = Object.keys(this.query[field]);

                for (const op of ops) {
                    switch (op) {
                        case 'Not':
                            condition[field] = Not(this.query[field][op]);
                            break;
                        case 'LessThan':
                            condition[field] = LessThan(this.query[field][op]);
                            break;
                        case 'MoreThan':
                            condition[field] = MoreThan(this.query[field][op]);
                            break;
                        case 'Equal':
                            condition[field] = Equal(this.query[field][op]);
                            break;
                        case 'MoreThanOrEqual':
                            condition[field] = MoreThanOrEqual(this.query[field][op]);
                            break;
                        case 'LessThanOrEqual':
                            condition[field] = LessThanOrEqual(this.query[field][op]);
                            break;
                        case 'Like':
                            condition[field] = Like(this.query[field][op]);
                            break;
                        case 'Any':
                            condition[field] = Any(this.query[field][op].trim().split(','));
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        return condition;
    }

}
