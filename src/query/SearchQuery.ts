import { Pagination } from './Pagination';

export class SearchQuery {
    public condition: object;
    public pagination: Pagination;
}
