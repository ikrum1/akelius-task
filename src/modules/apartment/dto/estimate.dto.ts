export class EstimateApartmentDto {
    public apartmentSize: number;
    public apartmentType?: string;
    public zipCode: number;
}
