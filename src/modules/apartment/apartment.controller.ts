import { Controller, Get, Post, Req, UseInterceptors } from '@nestjs/common';
import { ApartmentService } from './apartment.service';
import { Apartment } from './apartment.model';
import { FileInterceptor } from '@nestjs/platform-express';

import { SuccessResponse } from '../../responses';
import { SearchQuery } from '../../query/SearchQuery';
import { ApartmentQuery } from './decorators/ApartmentQuery';
import { ValidApartmentsData } from './decorators/ValidApartmentsData';
import { EstimateApartment } from './decorators/EstimateApartment';
import { EstimateApartmentDto } from './dto/estimate.dto';

@Controller('apartments')
export class ApartmentController {
  constructor (private apartmentService: ApartmentService) { }

  @Get('/')
  public async apartmentList(@Req() req: any, @ApartmentQuery() searchQuery: SearchQuery): Promise<SuccessResponse> {
    const queryResult = await this.apartmentService.findAll(searchQuery);
    const data = {
      page: searchQuery.pagination.page,
      totalAvailable: queryResult.count,
      rowReturned: queryResult.rows.length,
      rows: queryResult.rows,
    };
    return new SuccessResponse('Apartment list', data);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  public async uploadFile(@ValidApartmentsData() apartmentsData: Apartment[]): Promise<SuccessResponse> {
    await this.apartmentService.saveAll(apartmentsData);
    return new SuccessResponse('File imported successfully');
  }

  @Get('/estimateRent')
  public async estimate(@Req() req: any, @EstimateApartment() estimateApartmentDto: EstimateApartmentDto): Promise<SuccessResponse> {
    const queryResult = await this.apartmentService.estimateRent(estimateApartmentDto);
    return new SuccessResponse('Estimated rent for the apartment', queryResult);
  }

}
