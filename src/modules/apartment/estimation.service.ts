import { EstimateApartmentDto } from './dto/estimate.dto';
import { BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Apartment } from './apartment.model';
import { Repository } from 'typeorm';
import { EstimationServiceInterface } from './estimation.service.interface';
import { EstimatedRent } from './estimatedRent.interface';

export class EstimationService implements EstimationServiceInterface {
  constructor (
    @InjectRepository(Apartment)
    private readonly estimationRepository: Repository<Apartment>
  ) { }

  public async estimateRent(estimateApartmentDto: EstimateApartmentDto): Promise<EstimatedRent> {
    let typeCondition = '';

    if (estimateApartmentDto.apartmentType) {
      typeCondition = `AND apartment_type='${escape(estimateApartmentDto.apartmentType)}'`;
    }
    /*
      I have very limited idea about how companies estimateRent rent in real life.
      My approach is to find the average for the area and multiply by the inputSize
      But I observed that the price range / average differs depending on the size and apartment type
      If average doesn't really depends on the apartment size,
      then fallback is the most straight forward solution and using ORM might be better option rather RAW sql
     */
    const sql = `select COALESCE(
        -- calculate rent within + - 10 sqm
          (
            select (SUM(rent)/SUM(apartment_size)) as rent_pers_sqm from apartments
            WHERE zip_code = ${estimateApartmentDto.zipCode}
            AND apartment_size >= ${estimateApartmentDto.apartmentSize - 10}
            AND apartment_size <= ${estimateApartmentDto.apartmentSize + 10}
            AND deleted = false
            ${typeCondition}
          ),
        -- If there is no record found by the above condition
        -- calculate rent within + - 30 sqm
          (
            select (SUM(rent)/SUM(apartment_size)) as rent_pers_sqm from apartments
            WHERE zip_code = ${estimateApartmentDto.zipCode}
            AND apartment_size >= ${estimateApartmentDto.apartmentSize - 30}
            AND apartment_size <= ${estimateApartmentDto.apartmentSize + 30}
            AND deleted = false
            ${typeCondition}
          ),

        -- fallback if above two doesn't have any result
          (
            select (SUM(rent)/SUM(apartment_size)) as rent_pers_sqm from apartments
            WHERE zip_code = ${estimateApartmentDto.zipCode}
            AND deleted = false
            ${typeCondition}
          )
        ) as price_per_sqm
    `;

    const result = await this.estimationRepository.manager.query(sql);

    if (!result[0].price_per_sqm) {
      throw new BadRequestException(
        'Unable to estimateRent the rent with given parameters',
        'No relevant data available in the database to estimateRent'
      );
    }

    return {
      apartmentSize: estimateApartmentDto.apartmentSize,
      rentPerSquareMeter: Number(result[0].price_per_sqm),
      estimatedRent: result[0].price_per_sqm * estimateApartmentDto.apartmentSize,
    };
  }
}
