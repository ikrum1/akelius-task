import { EstimateApartmentDto } from './dto/estimate.dto';
import { EstimatedRent } from './estimatedRent.interface';

export interface EstimationServiceInterface {
    estimateRent(estimateApartmentDto: EstimateApartmentDto): Promise<EstimatedRent>;
}
