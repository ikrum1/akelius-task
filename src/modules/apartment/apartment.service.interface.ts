import { SearchQuery } from '../../query/SearchQuery';
import { QueryResult } from '../../query/QueryResult';
import { Apartment } from './apartment.model';

export interface IApartmentService {
    findAll(searchQuery: SearchQuery): Promise<QueryResult>;
    saveAll(apartments: Apartment[]): Promise<QueryResult>;
}
