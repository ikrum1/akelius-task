import { BadRequestException, createParamDecorator } from '@nestjs/common';
import { Apartment } from '../apartment.model';
import csv from 'csvtojson/v2';
import Joi from '@hapi/joi';

const validationSchema = Joi.object({
    apartment_id: Joi.string()
        .regex(new RegExp(`^[a-z0-9]{8}(\\-[a-z0-9]{4}){3}\\-[a-z0-9]{12}$`))
        .required()
        .error(() => new Error('Value for apartment_id is invalid')),

    zip_code: Joi.number()
        .min(1000)
        .max(99999)
        .required()
        .error(() => new Error('Value for zip_code is invalid')),

    apartment_type: Joi.string()
        .required()
        .error(() => new Error('Value for apartment_type is invalid')),

    apartment_size: Joi.number()
        .required()
        .error(() => new Error('Value for apartment_size is invalid')),

    rent: Joi.number()
        .required()
        .error(() => new Error('Value for rent is invalid')),

    deleted: Joi.bool()
        .optional()
        .error(() => new Error('Value for deleted is invalid')),
});

// tslint:disable-next-line:variable-name
export const ValidApartmentsData = createParamDecorator(
    async (data, req): Promise<Apartment[]> => {
        const csvData: object[] = await (csv().fromString(req.file.buffer.toString()));
        if (csvData.length === 0) {
            throw new BadRequestException('Empty file is not allowed');
        }

        const apartmentData: Apartment[] = csvData.map((item: any, index: number): Apartment => {
            const result = validationSchema.validate(item);
            if (result.error) {
                throw new BadRequestException(result.error.message, `Invalid data at line ${index + 2}`);
            }

            return Object.assign(new Apartment(), {
                apartmentId: item.apartment_id,
                zipCode: Number(item.zip_code),
                apartmentType: item.apartment_type,
                apartmentSize: Number(item.apartment_size),
                rent: Number(item.rent),
                deleted: item.deleted === 'true',
            });
        });

        return apartmentData;
    }
);
