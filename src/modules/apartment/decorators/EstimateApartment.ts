import { BadRequestException, createParamDecorator } from '@nestjs/common';
import { EstimateApartmentDto } from '../dto/estimate.dto';
import Joi from '@hapi/joi';

const validationSchema = Joi.object({
    zipCode: Joi.number()
        .min(1000)
        .max(99999)
        .required()
        .error(() => new Error('Value for zipCode is invalid')),

    apartmentType: Joi.string()
        .optional()
        .error(() => new Error('Value for apartmentType is invalid')),

    apartmentSize: Joi.number()
        .min(1)
        .max(5000)
        .required()
        .error(() => new Error('Value for apartmentSize is invalid')),
});

// tslint:disable-next-line:variable-name
export const EstimateApartment = createParamDecorator(
    async (data, req): Promise<EstimateApartmentDto> => {

        if (Object.keys(req.query).length === 0) {
            throw new BadRequestException('Missing required query parameters');
        }

        const validationResult = validationSchema.validate(req.query);
        if (validationResult.error) {
            throw new BadRequestException(validationResult.error.message);
        }

        return {
            apartmentSize: Number(req.query.apartmentSize),
            zipCode: Number(req.query.zipCode),
            apartmentType: req.query.apartmentType ? req.query.apartmentType : undefined,
        } as EstimateApartmentDto;
    }
);
