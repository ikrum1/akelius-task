import { createParamDecorator } from '@nestjs/common';
import { SearchQuery } from '../../../query/SearchQuery';
import { SearchQueryHelper } from '../../../query/SearchQueryHelper';

// tslint:disable-next-line:variable-name
export const ApartmentQuery = createParamDecorator(
    async (data, req): Promise<SearchQuery> => {
        const searchableFields = ['apartmentId', 'zipCode', 'apartmentType', 'apartmentSize', 'rent'];
        const builder = new SearchQueryHelper(searchableFields, req.query);
        return builder.getSearchQuery();
    }
);
