export interface EstimatedRent {
    apartmentSize: number;
    rentPerSquareMeter: number;
    estimatedRent: number;
}
