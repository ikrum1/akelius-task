import {
  Column,
  Entity,
  CreateDateColumn, PrimaryColumn, UpdateDateColumn,
} from 'typeorm';

@Entity('apartments')
export class Apartment {
  @PrimaryColumn()
  public apartmentId: string;

  @Column()
  public zipCode: number;

  @Column()
  public apartmentType: string;

  @Column()
  public apartmentSize: number;

  @Column()
  public rent: number;

  @CreateDateColumn({ type: 'timestamp' })
  public createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt: Date;

  @Column()
  public deleted: boolean;
}
