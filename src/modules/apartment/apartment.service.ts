import { Service } from 'typedi';
import { InjectRepository } from '@nestjs/typeorm';
import { Apartment } from './apartment.model';
import { Repository } from 'typeorm';
import { QueryResult } from '../../query/QueryResult';
import { SearchQuery } from '../../query/SearchQuery';
import { IApartmentService } from './apartment.service.interface';
import { EstimationService } from './estimation.service';

@Service()
export class ApartmentService extends EstimationService implements IApartmentService {
  constructor (
    @InjectRepository(Apartment)
    private readonly repository: Repository<Apartment>
  ) {
    super(repository);
  }

  public async findAll(searchQuery: SearchQuery): Promise<QueryResult> {
    const [rows, count] = await this.repository.findAndCount({
      where: Object.assign(searchQuery.condition, {deleted: false}),
      skip: searchQuery.pagination.offset,
      take: searchQuery.pagination.limit,
    });

    return { count, rows } as QueryResult;
  }

  /**
   *
   * @param Array<Apartment>
   */
  public async saveAll(apartments: Apartment[]): Promise<QueryResult> {
    const queryRunner = this.repository.manager.connection.createQueryRunner();
    await queryRunner.startTransaction();

    // either update all record, or not
    try {
      for (const apartment of apartments) {
        await queryRunner.manager.save(apartment);
      }
      await queryRunner.commitTransaction();
    } catch (e) {
      await queryRunner.rollbackTransaction();
      throw e;
    }

    return { affectedRows: apartments.length };
  }
}
