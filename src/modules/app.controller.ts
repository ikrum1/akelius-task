import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { SuccessResponse } from '../responses';

@Controller()
export class AppController {
    constructor (private readonly appService: AppService) {
    }

    @Get()
    public homePage(): SuccessResponse {
        return this.appService.homePage();
    }
}
