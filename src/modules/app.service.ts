import { Injectable } from '@nestjs/common';
import { SuccessResponse } from '../responses';

@Injectable()
export class AppService {
    public homePage(): SuccessResponse {
        return new SuccessResponse('Akelius Apartment API', []);
    }
}
