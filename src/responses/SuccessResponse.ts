export class SuccessResponse {
    public status: string;
    public message: string;
    public data: object;

    constructor (message: string, data?: object) {
        this.status = 'success';
        this.message = message;
        this.data = data || {};
    }
}
