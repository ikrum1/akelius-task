export class ErrorResponse {
    public status: string;
    public errorName: string;
    public message: string;
    public developerMessage: string;

    constructor (errorName: string, message: string, developerMessage?: string) {
        this.status = 'error';
        this.errorName = errorName;
        this.message = message;
        this.developerMessage = developerMessage || undefined;
    }

    public setDeveloperMessage(msg: string): void {
        this.developerMessage = msg;
    }
}
