import { ExceptionFilter, Catch, HttpException, ArgumentsHost, HttpStatus, InternalServerErrorException } from '@nestjs/common';
import { ErrorResponse } from '../responses/ErrorResponse';

@Catch()
export class AppExceptionFilter implements ExceptionFilter {
    public catch(error: Error, host: ArgumentsHost): void {
        const response = host.switchToHttp().getResponse();
        const exception = this.mapError(error);
        const exceptionResponse = exception.getResponse() as { statusCode: number, error: string, message: string };

        const jsonResponse = new ErrorResponse(exception.constructor.name, exceptionResponse.message, exceptionResponse.error);
        response.status(exception.getStatus()).json(jsonResponse);
    }

    private mapError(error: Error): HttpException {
        if (error instanceof HttpException) {
            return error;
        } else {
            return new InternalServerErrorException(error.message, error.constructor.name);
        }
    }
}
