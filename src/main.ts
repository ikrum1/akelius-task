import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';

import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { AppExceptionFilter } from './Filters/AppExceptionFilter';

async function bootstrap(): Promise<void> {
    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule
        // new FastifyAdapter({ logger: true }),
    );

    app.useGlobalFilters(new AppExceptionFilter());
    await app.listen(3000);
}

bootstrap().then().catch(console.log);
