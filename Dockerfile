FROM node:10-alpine

RUN apk --no-cache add python make g++

WORKDIR /app
COPY package.json ./
COPY .env ./
RUN npm install
COPY src ./src
COPY tsconfig.json ./
COPY tsconfig.build.json ./

RUN npm run build

EXPOSE 3000

CMD [ "node", "./dist/src/main.js" ]
